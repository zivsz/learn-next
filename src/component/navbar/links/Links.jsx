"use client";

import React, { useState } from "react";
import styles from "./links.module.css";
import Link from "next/link";
import { usePathname } from "next/navigation";

const links = [
  {
    title: "Homepage",
    path: "/",
  },
  {
    title: "About",
    path: "/about",
  },
  {
    title: "Contact",
    path: "/contact",
  },
  {
    title: "Blog",
    path: "/blog",
  },
];

const Links = () => {
  const pathName = usePathname();
  const session = true;
  const isAdmin = true;

  const [open, setOpen] = useState(false);

  return (
    <div className={styles.link_container}>
      <div className={styles.links}>
        {links.map((link) => (
          <Link
            className={`${styles.link_container} ${
              pathName === link.path && styles.link_active
            }`}
            href={link.path}
            key={link.title}
          >
            {link.title}
          </Link>
        ))}
        {session ? (
          <>
            {isAdmin && (
              <Link
                className={`${styles.link_container} ${
                  pathName === "/admin" && styles.link_active
                }`}
                href="/admin"
                key="Admin"
              >
                Admin
              </Link>
            )}
            <button className={styles.logout}>Logout</button>
          </>
        ) : (
          <Link
            className={`${styles.link_container} ${
              pathName === "/login" && styles.link_active
            }`}
            href="/login"
            key="Login"
          >
            Login
          </Link>
        )}
      </div>
      <button
        className={styles.menu_button}
        onClick={() => setOpen((prev) => !prev)}
      >
        Menu
      </button>
      {open && (
        <div className={styles.mobile_links}>
          {links.map((link) => (
            <Link
              className={`${styles.link_container} ${
                pathName === link.path && styles.link_active
              }`}
              href={link.path}
              key={link.title}
            >
              {link.title}
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};

export default Links;
